#include <stdio.h>
#include <stdlib.h>

extern char* base;


struct console {
	int max_col;
	int max_row;
	void* memory;
};


struct console* console__create(int max_col, int max_row){
	struct console *temp = malloc(sizeof(struct console));
	temp->max_col = max_col;
	temp->max_row = max_row;
	temp->memory = malloc(max_row*max_col*2);
	return temp;
}





void console__output( struct console* c){
	int i,j;
	for(i=0;i <= c->max_col+1; i++){
		putchar('*');
	}
	for(j=0;j<c->max_row; j++){
		putchar('\n');
		putchar('*');
		for(i=0; i< c->max_col;i++){
			char chap;
			char* mem = (char*) (c->memory + ((j*c->max_col+i)*2));
			chap  = *(mem);
			chap = chap>32 ? chap : ' ';
			chap = chap<127 ? chap : ' ';
			putchar(chap);
		}
		putchar('*');
	}
	putchar('\n');
	for(i=0;i <= c->max_col+1; i++){
		putchar('*');
	}
	putchar('\n');
}
void destroy_console( struct console* c){
	free(c->memory);
	free(c);
}

int main(int argc, char**argv){
 char* phrases[5];
 phrases[0] = "Hello World!! ";
 phrases[1] = "Kage Bushin No Jitsu. ";
 phrases[2] = "Since you have gone i feel like. ";
 phrases[3] = "Dust in the Wind. ";
 phrases[4] = "\n";
 phrases[5] = "\n";
 struct console* c = console__create(80,25);
 base = c->memory;
 console_reset();
 int i = 0;
 for (;i < 10; i++){
  int r = random()%5;
  printf("%s\n",phrases[r]);
  print_string(phrases[r]);
 }
 printf("\n");
 console__output(c);
 console_reset();
 console__output(c);
 return 0;
}