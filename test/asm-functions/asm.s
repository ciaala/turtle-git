

.globl basic_function
.type basic_function, @function
basic_function:
	
	pushq 	%rbp
	
	movq	%rsp, %rbp

	movq 	$4, %r8
	movq	$pointer, %rbx
	movq	pointer, %rcx
	movq	pointer2, %rdx
	movq	%r8, (%rcx)
	movq	%rbx, (%rdx)

	leave
	retq

