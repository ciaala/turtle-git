	.file	"main.c"
	.comm	process_head,8,8
	.comm	five,20,16
	.text
.globl main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	movq	%rsp, %rbp
	.cfi_offset 6, -16
	.cfi_def_cfa_register 6
	leave
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
.globl access_element
	.type	access_element, @function
access_element:
.LFB1:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	movq	%rsp, %rbp
	.cfi_offset 6, -16
	.cfi_def_cfa_register 6
	movl	five+12(%rip), %eax
	movl	%eax, -4(%rbp)
	movl	-4(%rbp), %eax
	cltq
	movl	five(,%rax,4), %eax
	movl	%eax, five+4(%rip)
	leave
	ret
	.cfi_endproc
.LFE1:
	.size	access_element, .-access_element
.globl storersp
	.type	storersp, @function
storersp:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	movq	%rsp, %rbp
	.cfi_offset 6, -16
	.cfi_def_cfa_register 6
	movq	process_head(%rip), %rax
	movq	$4, 392(%rax)
	leave
	ret
	.cfi_endproc
.LFE2:
	.size	storersp, .-storersp
	.ident	"GCC: (Gentoo 4.4.3-r2 p1.2) 4.4.3"
	.section	.note.GNU-stack,"",@progbits
