#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int DEBUG =0;
// 

#define print_string console_print_string

extern char* _64_base;


struct console {
    int max_col;
    int max_row;
    void* memory;
};


struct console* console__create(int max_col, int max_row) {
    struct console *temp = malloc(sizeof(struct console));
    temp->max_col = max_col;
    temp->max_row = max_row;
    temp->memory = malloc(max_row*max_col*2);
    return temp;
}

void console__output( struct console* c) {
    int i,j;
    for (i=0;i <= c->max_col+1; i++) {
        putchar('*');
    }
    for (j=0;j<c->max_row; j++) {
        putchar('\n');
        putchar('*');
        for (i=0; i< c->max_col;i++) {
            char chap;
            char* mem = (char*) (c->memory + ((j*c->max_col+i)*2));
            chap  = *(mem);
            chap = chap>32 ? chap : ' ';
            chap = chap<127 ? chap : ' ';
            putchar(chap);
        }
        putchar('*');
    }
    putchar('\n');
    for (i=0;i <= c->max_col+1; i++) {
        putchar('*');
    }
    putchar('\n');
}
void destroy_console( struct console* c) {
    free(c->memory);
    free(c);
}
void print(struct console* c, int i) {
    char* phrases[8];
    phrases[0] = "Hello World!! ";
    phrases[1] = "Kage Bushin No Jutsu. \n";
    phrases[2] = "Since you have gone I feel like I've gotten older. ";
    phrases[3] = "Dust in the Wind. ";
    phrases[4] = "\rILDJIT Compiler. ";
    phrases[5] = "Avatar\n";
    phrases[6] = "\nIronman2";
    phrases[7] = "\n";
    print_string(phrases[i]);
    if (DEBUG)
    {
        printf("%s\n",phrases[i]);
        console__output(c);
    }

}
int main(int argc, char**argv) {

    struct console* c = console__create(80,25);
    console_initialise(c->memory);
    int i = 0;
    DEBUG = 0;
    for(i=0; i<12; i++){
        print(c,1);
        print(c,5);
    }
    unsigned int seed = time(NULL);
    srandom(seed);
    DEBUG = 1;
    for (i=0;i < 55; i++) {
        int r = random()%6;
        print(c,r);
    }
    
    printf("\n");
    console__output(c);
    console_reset();
    console__output(c);
    free(c);
    return 0;
}



// kate: indent-mode cstyle; space-indent on; indent-width 0; 
