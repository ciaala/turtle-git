#include "stdio.h"
typedef union Capozzo{
	char words[8];
	unsigned long long int _64bit;
	struct{
		int low32bit;
		int high32bit;
	};
}Capozzo;

int main(int argc, char** argv){
	Capozzo a;
	a.words[0] = '0';
	a.words[1] = '0';
	a.words[2] = '0';
	a.words[3] = '0';
	a.words[4] = 'Z';
	a.words[5] = 'Z';
	a.words[6] = 'Z';
	a.words[7] = 'Z';
	printf("64bits:\t%llx\nlow:\t%x\nhigh:\t%x\n",a._64bit,a.low32bit,a.high32bit);
	return 0;
}