#include <stdio.h>
typedef unsigned long long _64bit;
int
main(int argc, char** argv){
	if (argc == 2){
		 _64bit address;
		sscanf ( argv[1], "%p", &address);
		_64bit index1 = address / 0x8000000000;
		_64bit index2 = (address % 0x8000000000) / 0x40000000;
		_64bit index3 = (address % 0x40000000) / 0x200000;
		printf( "address: %p\n", address);
		printf( "[0] = %lld\n[1] = %lld\n[2] = %lld\n", index1, index2, index3);
	}
	return 0;
}