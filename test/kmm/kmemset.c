#include "../../include/types.h"
#include "stdio.h"
void
kmemset(_64bit* address, _64bit size, _64bit value){
	while( size > 0 ){
		*address = value;
		address++;
		size--;
	}
}


int main(int argc, char**argv){
	char buffer[0x801];
	kmemset( buffer, 0x100, 0x6363636363636363);
	buffer[800] = '\0';
	printf("%s\n",buffer);
	return 0;
}

