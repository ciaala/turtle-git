
#include "pt4.h"
#include <stdio.h>
#include <stdlib.h>
mm_page_table mm_main_paging_table[30];

void check_structure_size(){
	printf("mm_page_table: %d\nmm_page_table_row: %d", (int) sizeof(mm_page_table),(int)sizeof(mm_page_table_row));
}
void* get_address(_64bit* row){
	long long int result = row->field;
	result &=  0xFFFFFFFFF000;
	return (void*)result;
}

void print_map(mm_page_table* pml4, int depth ){
	char e = ' ';
	int i;
	char format[32];
	snprintf(format,32, "%s%d%s","%d%", depth, "c%llx");
	for (i=0; i < ROWS; i++){
		
		if ( pml4->rows[i].field != 0 ){
			printf(format, i, pml4->rows[i]);
			if (depth < 3){
				print_map(get_address(&pml4->rows[i]), depth++);
			}
		}
			
	}
}

int main(int argc, char** argv){
	check_structure_size();
	mm_setup_identity_map();
	print_map(mm_main_paging_table,0);
	return 0;
}