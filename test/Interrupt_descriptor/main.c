// typedef unsigned int Bit32u;
// typedef unsigned char Bit8u;
// typedef unsigned char Bit16u;
// typedef unsigned char bx_bool;
// bx_address
#include "types.h"
#include "stdio.h"
#include "kernel/interrupts.h"
#include "kernel/process.h"
/*

unsigned ist:           3;
unsigned _reserved0:    5;
unsigned type:          4;
unsigned _reserved1:    1;
// Descriptor Privilege Level
unsigned dpl:           2;
unsigned p:             1;

*/
int
main(int argc, char** argv) {

	printf("sizeof(interrupt_gate): %lu\n",sizeof(interrupt_gate));
	printf("sizeof(register_stack): %lu\n",sizeof(register_stack));
	return 0;
}
