# We added os/turtle.os to phony to force 'make' to check if it needs a recompile

MAKEFLAGS=--no-print-directory --warn-undefined-variables
.PHONY: os/turtle.os

turtle.iso: os/turtle.os
	@printf "@iso\n"
	@cp os/turtle.os iso/turtle	
	@grub-mkrescue --modules="multiboot configfile iso9660 sh help minicmd fshelp boot pc" --overlay=iso turtle.iso

os/turtle.os :
	@printf "@kernel\n"
	@$(MAKE) -C os

.PHONY: clean os/turtle.os turtle.iso

clean:
	@rm -f turtle.iso
	@$(MAKE) -C os clean