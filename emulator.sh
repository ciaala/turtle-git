#!/bin/sh
date
if test "$1" = "R"; then
	qemu-system-x86_64 -m 1024 --cdrom turtle.iso
else 	
	echo "GDB Debug Stub ON"
	qemu-system-x86_64 -m 1024 -s -S -serial mon:tcp::7777,server --cdrom turtle.iso &
	sleep 1
	cd os
	xterm -geometry 256x40 -rv "gdb" &
	cd ..
	echo "Netcat Connection"
	nc localhost 7777
	for i in `jobs -p`;
		do 
			kill $i
		done
fi
