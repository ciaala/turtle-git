TARGET_CC=x86_64-pc-linux-gnu-gcc
TARGET_CFLAGS=-Wall -W -Wshadow -Wpointer-arith -Wmissing-prototypes                  -Wundef -Wstrict-prototypes -g -Os -falign-jumps=1 -falign-loops=1 -falign-functions=1 -mno-mmx -mno-sse -mno-sse2 -mno-3dnow -fno-dwarf2-cfi-asm -m32 -fno-stack-protector -mno-stack-arg-probe
TARGET_ASFLAGS=
TARGET_CPPFLAGS=-isystem=./include -I./include -I. -I./include -Wall -W -I/lib64/grub/i386-pc -I/usr/include
STRIP=x86_64-pc-linux-gnu-strip
OBJCONV=
TARGET_MODULE_FORMAT=elf32
TARGET_APPLE_CC=0
COMMON_ASFLAGS=-nostdinc -fno-builtin -m32
COMMON_CFLAGS=-fno-builtin -mrtd -mregparm=3 -m32
COMMON_LDFLAGS=-m32 -nostdlib
