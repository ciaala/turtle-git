#!/bin/sh
set -m
/usr/local/bin/bochs -qf bochs-gdb-stub.cfg &
sleep 1
cd os
xterm -geometry 200x80 -rv "gdb" &
fg 1
cd ..
for i in `jobs -p`;
do
	kill $i;
done

