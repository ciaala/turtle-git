#include <boot/multiboot.h>
#include <servers/memory_manager.h>

#include <drivers/early_console_64.h>

_32bit kalloc_memory_array[16384];
_32bit* kalloc_memory_pointer;
memory_manager mm;


void
mm_print_freelist();
void
mm_import_memory_map(struct grub_multiboot_info *mb_info);




memory_manager*
memory_manager_initialise(struct grub_multiboot_info *mb_info){
	kalloc_memory_pointer = kalloc_memory_array;
	mm.freelist.first = NULL;
	mm.freelist.length = 0;
	mm.usedlist.first = NULL;
	mm.usedlist.length = 0;
	mm_import_memory_map(mb_info);
	mm_print_freelist();
	return &mm;
}

void
mm_import_memory_map(struct grub_multiboot_info *mb_info){
	grub_uint64_t total_available = 0;
	struct grub_multiboot_mmap_entry* mmap = (struct grub_multiboot_mmap_entry*) (_64bit)mb_info->mmap_addr;
	_64_println_string("Memory Map");
	while( (_64bit) mmap < ( mb_info->mmap_addr + mb_info->mmap_length)){
		_64_print_string("| ");
		_64_print_hex( mmap->addr );
		_64_print_string("-");
		_64_print_hex( mmap->addr + mmap->len);
		_64_print_string( " size: ");
		_64_print_int( mmap->len / 1024 );
		_64_print_string( " KB ");
		if ( mmap->type == GRUB_MULTIBOOT_MEMORY_RESERVED ){
			_64_println_string( "RESERVED" );
		} else if ( mmap->type == GRUB_MULTIBOOT_MEMORY_AVAILABLE ){
			_64_println_string( "AVAILABLE" );
			total_available += mmap->len/1024;
			/** Add memory to the free list */
			if (mmap->len > 0x200000){
				_64bit address = (mmap->addr | 0x200000);
				_64bit low_address = (address - (address % 0x200000));
				address = (mmap->addr + mmap->len);
				_64bit high_address = (address - (address % 0x200000));
				if ( (high_address - low_address) > 0x200000){
					memory_list_add(&mm.freelist, low_address, high_address);
				}
			}
		} else {
			_64_print_string( "UNKNOWN " );
			_64_print_int( mmap->type );
			_64_print_string("\n");
		}
		mmap = (struct grub_multiboot_mmap_entry*) ( (grub_uint64_t) mmap + mmap->size + sizeof(grub_uint32_t) );
	}
	_64_print_string("| Total Available Memory: ");
	_64_print_int( total_available );
	_64_println_string( " KB");
}

void
mm_print_freelist(){
	_64_println_string("Memory Freelist");
	memory_list_element* e = mm.freelist.first;
	while (e){
		_64_print_string("| ");
		_64_print_hex( e->low_address );
		_64_print_string( "-" );
		_64_print_hex( e->high_address );
		_64_print_string(" len: ");
		_64_print_hex( e->size );
		_64_print_string("\n");
		
		e = e->next;
	}
}



void*
kalloc_stack(){
	return kalloc_page() + PAGE_SIZE;
}

void*
kalloc_page(){
	_64bit result;
	memory_list_element* e = mm.freelist.first;
	while (e && (e->size < PAGE_SIZE)){
		e = e->next;
	}
	if (e) {
		result = e->low_address;
		if ( e->size != PAGE_SIZE ) {
			e->low_address += PAGE_SIZE;
			e->size -= PAGE_SIZE;
			
		} else {
			e->prev->next = e->next;
			kfree(e);
		}
		
	} else {
		_64_println_string("ERROR: out of memory");
		while(1){
		}
	}
	_64_print_string(" <mem ");
	_64_print_hex(result);
	_64_print_string("-");
	_64_print_hex(e->low_address);
	_64_print_string(">");
	kmm_map_page(result);
	kmemset((_64bit*)result,PAGE_SIZE,0);
	return (void*)result;
}

void*
kalloc(_32bit length){
	void* result = kalloc_memory_pointer;
	kalloc_memory_pointer += length;
 	return result; 
}

void
kfree(void* address){
	// TODO we need a proper memory manager
}

void
kmemcpy( char* source, char* target, _64bit size) {
	while ( size > 0 ) {
		size--;
		*target = *source;
		target++;
		source++;
	}
}

void
kmemcpy8( _64bit* source, _64bit* target, _64bit size){
	while ( size > 0 ) {
		size-=8;
		*target = *source;
		target++;
		source++;
	}
}

void
kmemset( char* address, _64bit size, _8bit value) {
	while ( size > 0 ) {
		size--;
		*address = value;
		address++;
	}
}

void
kmemset8(_64bit* address, _64bit size, _64bit value){
	while( size > 0 ){
		*address = value;
		address ++;
		size -= 8;
	}
}
#define PAGE_rw_WRITABLE 0x3llu
#define PAGE_us_SUPERVISOR 0x4llu

_64bit
kmm_build_2MB_page_row(_64bit address, _32bit low_flag, _64bit high_flag) {
	_64bit result;
	_64bit address_mask = 0xffffe00000;
	low_flag = low_flag | 0x80;
	result = (address & address_mask) | low_flag | high_flag;
	return result;
}

/** identity mapping */
void
kmm_map_page(_64bit address){
	_64bit row_value = kmm_build_2MB_page_row(address, PAGE_rw_WRITABLE | PAGE_us_SUPERVISOR, 0);
	_64bit index1 = address / 0x8000000000;
	_64bit index2 = (address % 0x8000000000) / 0x40000000;
	_64bit index3 = (address % 0x40000000) / 0x200000;
	mm_page_table* table = (mm_page_table*) (mm_main_paging_table.rows[index1] & 0xFFFFFFF000);
	_64_print_hex(table);
	_64_print_string(" i1:");
	_64_print_int(index1);
	_64_print_string(" ");
	if (table != NULL) {
		
		table = (mm_page_table*) (table->rows[index2] & 0xFFFFFFF000);
		_64_print_hex(table);
		_64_print_string(" i2:");
		_64_print_int(index2);
		_64_print_string(" i3:");
		if (table != NULL){
			_64_print_int(index3);
			table->rows[index3] = row_value;
		}
	}
}

