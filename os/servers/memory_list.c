#include <servers/memory_manager.h>


memory_list_head*
memory_list_head_new(){
	memory_list_head* head = kalloc(sizeof(struct memory_list_head));
	head->first = NULL;
	head->length = 0;
	return head;
}

void
memory_list_add(memory_list_head* head, _64bit low_address,_64bit high_address){
	// EMPTY LIST
	if (head->length == 0) {
		head->length = 1;
		memory_list_element* element = kalloc(sizeof(memory_list_element)) ;
		element->low_address = low_address;
		element->high_address = high_address;
		element->size = high_address - low_address;
		element->next = NULL;
		element->prev = NULL;
		head->first = element;
		return;
	} else { 
		memory_list_element* c = head->first;
		memory_list_element* p = NULL;

		/**
			while the memory block should follow the current block 
		*/
		while ( c && (low_address > c->low_address)) {
			p = c;
			c = c->next;
		}
		/**
			the memory block ends before us
		*/
		if (c) {
			/** the memory block close a memory hole between the current and the previous */
			if ( p && c->low_address == high_address && p->high_address == low_address){
				p->next = c->next;
				p->high_address = c->high_address;
				p->size = p->high_address - p->low_address;
				kfree(c);
			} /** the block is immediately before us */
			else if (c->low_address == high_address) {
				c->low_address = low_address;
				c->size = c->high_address - c->low_address;
			} /** the block is immediately after the previous */
			else if (p && (p->high_address == low_address) ){
				p->high_address = high_address;
				p->size = p->high_address - p->low_address;
			} /** the block is between the previous and the current */
			else if ( p && c  ){
				memory_list_element* element = kalloc(sizeof(memory_list_element)) ;
				element->low_address = low_address;
				element->high_address = high_address;
				element->size = element->high_address - element->low_address;
				p->next = element;
				element->prev = p;
				element->next = c;
				c->prev = element;
				head->length++;				
			} /** the block should be placed as the first */
			else if (p) {
				
			}
		} else {
			memory_list_element* element = kalloc(sizeof(memory_list_element)) ;
			element->low_address = low_address;
			element->high_address = high_address;
			p->next = element;
			element->prev = p;
			element->size = element->high_address - element->low_address;
			head->length ++;
		}
	}
}