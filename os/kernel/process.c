#include "drivers/early_console_64.h"
#include "kernel/process.h"
#include "kernel/clock.h"
#include "servers/memory_manager.h"
#include "kernel/interrupts.h"
#include "amd64/klib.h"

_64bit pid_counter = 0;
process* process_head;
_64bit_tss tss;

typedef struct {
    _64bit schedule_counter;
} process_manager;

process_manager pm;


void process_idle() {
    _64bit counter = 0;
    _64bit a=1,b=24,c=12;
    _64_println_string("\nprocess initialized: idle");
    interrupt_hardware_enable();
    while (1) {
        interrupt_hardware_disable();
        _64_print_string("\r[idle] ");
        _64_print_hex(counter);
        counter += b / c - a;
        interrupt_hardware_enable();
        wait();
    }
}


void process_0() {
    _64_println_string("\nprocess initialized: p0");
    interrupt_hardware_enable();

    while (1) {
        interrupt_hardware_disable();
        _64bit time = os_clock_tick();
        _64_print_string("\r[   0] clock_tick: ");
        _64_print_hex(time);
        _64_print_string(" schedules: ");
        _64_print_hex(pm.schedule_counter);
        interrupt_hardware_enable();
        wait();
    }
}

void process_1() {
    _64_println_string("\nprocess initialized: p1");
    interrupt_hardware_enable();
    while (1) {
        interrupt_hardware_disable();
        _64bit time = os_clock_tick();
        _64_print_string("\r[   1] clock_tick: ");
        _64_print_hex(time);
        _64_print_string(" schedules: ");
        _64_print_hex(pm.schedule_counter);
        interrupt_hardware_enable();
        wait();
    }
}


void process_fake(){
    while(1){

    }
}
struct {
    _process_function function;
    char* name;
} process_map [] = {
    {process_idle,  "process idle"},
    {process_0,     "process    0"},
    {process_1,     "process    1"},
};

process*
create_process( char* name, _process_function function) {
    _64_print_string("| - ");
    _64_print_string(name);

    
    process *p = kalloc(sizeof(p));
    p->pid = pid_counter;
    pid_counter ++;
    p->start_time = os_clock_tick();
  
    //kmemset((_64bit*)&p->tss, sizeof(_64bit_tss), 0);
   //p->_init = function;
    p->status = PROCESS_READY;
    p->name = name;
    p->prev = NULL;
    p->next = NULL;
    p->stack = kalloc_stack();
    p->rsp = p->stack - 20;
    /**



        rsp[0] - rsp[3]
            rax rbx rcx rdx
        rsp[4] - rsp[6]
            rdi rsi rbp
        rsp[7] - rsp[14]
            r[0-9]
        rsp[15] - rsp[19]
            rip cs eflags rsp ss

    */
    /** rip */
    p->rsp[15] = function;
    /** cs */
    p->rsp[16] = asm_read_cs();
    /** rflag */
    p->rsp[17] = asm_read_rflag();
    /** rsp */
    p->rsp[18] = p->stack;
    /** ss */
    p->rsp[19] = asm_read_ss();
    _64_println_string(" ");
    return p;
    
}
void
pm_setup_tss_register();

void
process_initialise() {
    pm.schedule_counter = 0;
    _64_println_string("Process Manager Initialization");
    _64bit i = 0;
    _64bit size = 3;
    process_head = create_process( "process fake", process_fake );
    process *p = create_process(process_map[0].name, process_map[0].function );
    process *first = p;
    process* previous = first;
    process_head->next = first;
    for (i=1; i < size; i++) {
        p = create_process( process_map[i].name, process_map[i].function );
        p->prev = previous;
        previous->next = p;
        previous = p;
    }
    p->next = first;
    first->prev = p;
   // pm_setup_tss_register();
}

/**
    Guarantee Progress and in some way Fairness.
*/
void
pm_schedule() {
    if ( process_head->next ) {
        pm.schedule_counter ++;
        process_head = process_head->next;
    }
//     console_print_string(" >> scheduling: ");
//     console_println_string(process_head->name);
    console_println_string(" ");
}



void
pm_setup_cpu_x_process(process* p) {

}

void
pm_setup_tss_descriptor( tss_descriptor* td, _64bit type, _64bit dpl , _64bit g, _64bit available, _64bit offset, _64bit segment) {

    td->p = 1;
    td->type = type;
    td->available = available;
    td->dpl = dpl;
    td->segment_0_15 = segment;
    td->segment_16_19 = segment >> 16;
    td->offset_0_23 = offset;
    td->offset_24_63 = offset >> 24;
}

// void
// pm_setup_tss_register() {
//     kmemset(&tss, sizeof(_64bit_tss), 0 );
//     _64bit rsp0_stack = kalloc_stack() ;
//     pm_setup_tss_descriptor( &tss.rsp0, _64BIT_TSS_AVAILABLE, DPL_KERNEL, 0, 1, rsp0_stack, 0x8 );
//     asm_load_tss();
// }
// kate: indent-mode cstyle; space-indent on; indent-width 0; 
