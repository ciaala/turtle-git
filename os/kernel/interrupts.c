#include <kernel/interrupts.h>
#include <amd64/interrupts.h>
#include <amd64/klib.h>
#include <drivers/early_console_64.h>



void
io_wait(){
wait();
}




struct __interrupt_descriptor {
    _16bit length;
    struct interrupt_gate *table;
}  __attribute__((__packed__));



struct __interrupt_descriptor interrupt_descriptor;

void (*handlers[256])(void);


typedef  struct interrupt_gate {
    _64bit offset_0_15:     16;
    _64bit segment:         16;
    _64bit ist:             3;
    _64bit __reserved0:     5;
    _64bit type:            4;
    _64bit __reserved1:     1;
    // Descriptor Privilege Level
    _64bit dpl:             2;
    _64bit p:               1;
    _64bit offset_16_63:    48;
    _64bit __reserved2:     32;
}  __attribute__((__packed__)) interrupt_gate;


typedef struct tss_element {

} __attribute__((__packed__)) tss_element;




//int irq_actids[256];
interrupt_gate interrupt_descriptor_table[256];


typedef struct function_table_row {
    unsigned int id;
    void (*function)(void);
} function_table_row;

function_table_row interrupt_hardware_handlers_table [] = {
    {VECTOR(0), _hw_int_0},
    {VECTOR(1), _hw_int_1},
//     {VECTOR(2), _hw_int_2},
//     {VECTOR(3), _hw_int_3},
//     {VECTOR(4), _hw_int_4},
//     {VECTOR(5), _hw_int_5},
//     {VECTOR(6), _hw_int_6},
//     {VECTOR(7), _hw_int_7},
//     {VECTOR(8), _hw_int_8},
//     {VECTOR(9), _hw_int_9},
//     {VECTOR(10),_hw_int_10},
//     {VECTOR(11),_hw_int_11},
//     {VECTOR(12),_hw_int_12},
//     {VECTOR(13),_hw_int_13},
//     {VECTOR(14),_hw_int_14},
//     {VECTOR(15),_hw_int_15},
};

//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
void
interrupt_hardware_controller_reset() {
    interrupt_hardware_disable();
    // master
    output_byte(INT_CTL, ICW1_AT );
    // io_wait();
    output_byte(INT_CTLMASK, IRQ0_VECTOR);
    // io_wait();
    output_byte(INT_CTLMASK, (1 << CASCADE_IRQ));
    // io_wait();
    output_byte(INT_CTLMASK, ICW4_AT_MASTER);
    // io_wait();
    output_byte(INT_CTLMASK, ~(1<<CASCADE_IRQ));
    // io_wait();
    // slave
    output_byte(INT2_CTL, ICW1_AT);
    // io_wait();
    output_byte(INT2_CTLMASK, IRQ8_VECTOR);
    // io_wait();
    output_byte(INT2_CTLMASK, CASCADE_IRQ );
    // io_wait();
    output_byte(INT2_CTLMASK, ICW4_AT_SLAVE);
    // io_wait();
    output_byte(INT2_CTLMASK, ~0);
    // io_wait();
}


void interrupt_handler_register(_32bit number,_64bit function_address,_8bit ist, _8bit dpl_type) {
    console_print_string("[" );
    console_print_hex(number);
    console_print_string("]" );
    interrupt_gate* idt = &interrupt_descriptor_table[number];
    idt->dpl = dpl_type;
    idt->ist = ist;
    idt->segment = 0x8;
    idt->type = _64BIT_INTERRUPT_GATE;
    idt->offset_0_15 = function_address;
    idt->offset_16_63 = function_address >> 16;
    idt->p = 1;
    idt->__reserved0 = 0;
    idt->__reserved1 = 0;
    idt->__reserved2 = 0;
}

void interrupt_asm_handlers_setup() {

    int i;
    for (i=0; i<256; i++) {
        handlers[i] = NULL;
        interrupt_descriptor_table[i].p = 0;
    }
    int size = sizeof(interrupt_hardware_handlers_table)/sizeof(function_table_row);
    
    for (i=0; i<size; i++) {
        interrupt_handler_register(interrupt_hardware_handlers_table[i].id,(_64bit)interrupt_hardware_handlers_table[i].function, 0, DPL_KERNEL);
    }
//     int size = 256;
//     
//     for (i=0; i<size; i++) {
//         interrupt_handler_register(i,i, 0, DPL_KERNEL );
//     }
    interrupt_descriptor.length = 0x800;
    interrupt_descriptor.table = interrupt_descriptor_table;
    interrupt_hardware_load_idt();
}

void
interrupt_initialise() {
    _64_println_string("Interrupt Manager Iniatilization");
    interrupt_hardware_controller_reset();
    interrupt_asm_handlers_setup();
}
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////





/**

*/


void
interrupt_register(_8bit number, void (*handler)(void)) {
    handlers[number] = handler;
    interrupt_enable(number);
}

void
interrupt_unregister(_8bit number) {
    handlers[number] = NULL;
    interrupt_disable(number);
}

void
interrupt_handler(_8bit number) {

    if ( handlers[number] != NULL ) {
        void (*handler)(void) = handlers[number];
        handler();
    }
    /** Send an Ack to the controllers */
    output_byte(INT_CTL,END_OF_INT);
    if (IRQ8_VECTOR <= number) {
        output_byte(INT2_CTL,END_OF_INT);
    }
}


void
interrupt_enable(_8bit interrupt_number) {
    interrupt_hardware_disable();
    _16bit controller;
    if (interrupt_number < 8) {
        controller = INT_CTLMASK;
    } else if ( interrupt_number >= 8 && interrupt_number < 15 ) {
        controller = INT2_CTLMASK;
    }
    _8bit mask = input_byte(controller);
    mask &= ~(1 << interrupt_number);
    output_byte(controller, mask);
    
}

void
interrupt_disable(_8bit number){

}
// kate: indent-mode cstyle; space-indent on; indent-width 0; 
