#include <amd64/klib.h>
#include <kernel/interrupts.h>
#include <drivers/early_console_64.h>
#include <kernel/process.h>


#define TIMER0			0x40
#define TIMER_MODE		0x43
#define SQUARE_WAVE		0X36
#define TIMER_FREQ		1200000
#define HZ				10
#define TIMER_COUNT		((unsigned) (TIMER_FREQ/HZ))


_64bit clock_era = 0;
_64bit clock_timeout = 100;
_64bit clock_value = 0;
_64bit clock_tick = 50;

void clock_task(){
	clock_era++;
//  	_64_print_string("Clock Task: ");
// 	_64_print_int(clock_era);
//  	_64_println_string(" era");
	pm_schedule();
}
void
clock_handler(){
	clock_value++;
	if ( clock_value >= clock_timeout ){
		clock_timeout = clock_value + clock_tick;
		clock_task();
	}
}
void
clock_initialise(){
	_64_println_string("Clock Interrupt Handler Initialization");
	output_byte(TIMER_MODE, SQUARE_WAVE );
	output_byte(TIMER0, TIMER_COUNT );
	output_byte(TIMER0, TIMER_COUNT >> 8 );
	interrupt_register(CLOCK_IRQ,clock_handler);
}



_64bit
os_clock_tick(){
	return clock_value;
}
