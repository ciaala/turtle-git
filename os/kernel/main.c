#include <kernel/interrupts.h>
#include <kernel/clock.h>
#include <kernel/process.h>
#include <servers/memory_manager.h>
#include <drivers/early_console_64.h>

#include <boot/multiboot.h>


#define STRINGIFY_(x) #x
#define STRINGIFY(x) STRINGIFY_(x)
char* s64_COMPILE_TIME = STRINGIFY(__COMPILETIME__);


void init();

void turtle_main64(unsigned long long int bootloader_signature, struct grub_multiboot_info* mb_info){
	console_initialise((void*)0xb8000);
	console_print_string("Turtle(");
	console_print_string( s64_COMPILE_TIME);
	_64_println_string(") has switched to 64bit");
	/** Memory Manager */
	_64_println_string(NULL);
	memory_manager_initialise(mb_info);

	
	/** Interrupt and Clock Setup */
	_64_println_string(NULL);
	interrupt_hardware_disable();
	interrupt_initialise();
	clock_initialise();
	
	
	/** Process Start */
	_64_println_string(NULL);
	process_initialise();

	/** Journey to Userland Login */
	_64_println_string(NULL);
	_64_println_string("Enabling Interrupt Lines");
	
	init();
	_64_println_string("System stopped");
}

void init(){
	int i = 0;
	_64_println_string("Entering Kernel busy loop");
	while (1) {
		interrupt_hardware_enable();
// 		i ++;
// 		_64_print_string(" ");
// 		_64_print_hex(i);
// 		_64_println_string(" ");
// 		wait();
	}
}
