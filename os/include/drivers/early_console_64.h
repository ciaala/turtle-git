#include "types.h"
#ifndef EARLY_CONSOLE_64_H
#define EARLY_CONSOLE_64_H

/* early_console_64.c */
void _64_scroll_line(_32bit rows);
void _64_increment_line(void);
void _64_print_char(char c);
void _64_println_string(char *string);
void _64_print_string(char *string);
void ___64_print_number(_64bit value, int base);
void _64_console_reset(void);



void console_initialise(char* base_address);
void console_reset();
void console_print_char(char c);
void console_print_string(char* string);
void console_println_string(char* string);
void _console_print_number(_64bit number,_64bit base);

#define console_print_hex(number) \
	console_print_char('0'); console_print_char('x'); _console_print_number((_64bit)number,16)

#define console_print_int(number) \
	_console_print_number((_64bit)number,10)
	
#define _64_print_hex(number) \
	console_print_char('0'); console_print_char('x'); _console_print_number((_64bit)number,16)
	
#define _64_print_int(number) \
	_console_print_number((_64bit)number,10)


#endif