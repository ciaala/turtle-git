#include <types.h>
void output_byte(_16bit port, _8bit value );
_8bit input_byte(_16bit port );


void disable_interrupts();
void enable_interrupts();

_64bit asm_read_rflag();
_64bit asm_read_cs();
_64bit asm_read_ss();

void _io_wait();


void wait();