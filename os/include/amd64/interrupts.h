/**
    os/include/amd64/interrupts.h
    Macro definition for the assembler only file os/amd64//interrupts.s
*/


/* 8259A interrupt controller ports. */
#define INT_CTL             0x20    /* I/O port for interrupt controller */
#define INT_CTLMASK         0x21    /* setting bits in this port disables ints */
#define INT2_CTL            0xA0    /* I/O port for second interrupt controller */
#define INT2_CTLMASK        0xA1    /* setting bits in this port disables ints */

/* Magic numbers for interrupt controller. */
#define END_OF_INT          0x20    /* code used to re-enable after an interrupt */

#define IRQ0_VECTOR         0x50
#define IRQ8_VECTOR         0x70

#define CASCADE_IRQ         2       /* cascade enable for 2nd AT controller */



#define ICW1_AT             0x11    /* edge triggered, cascade, need ICW4 */
#define ICW1_PC             0x13    /* edge triggered, no cascade, need ICW4 */
#define ICW1_PS             0x19    /* level triggered, cascade, need ICW4 */
#define ICW4_AT_SLAVE       0x01    /* not SFNM, not buffered, normal EOI, 8086 */
#define ICW4_AT_MASTER      0x05    /* not SFNM, not buffered, normal EOI, 8086 */
#define ICW4_PC_SLAVE       0x09    /* not SFNM, buffered, normal EOI, 8086 */
#define ICW4_PC_MASTER      0x0D    /* not SFNM, buffered, normal EOI, 8086 */



#define VECTOR(irq)     \
    (((irq) < 8 ? IRQ0_VECTOR :  IRQ8_VECTOR) + ((irq) & 0x07))





// kate: indent-mode cstyle; space-indent on; indent-width 0; 
