#include "types.h"
#ifndef PROCESS_H

#define PROCESS_H
typedef struct tss_descriptor {
		_64bit segment_0_15:         16;
		_64bit offset_0_23:     24;
		/**
			Segment Type
			TSS_AVAILABLE
			TSS_BUSY
		*/
		_64bit type:            4;
		_64bit __zero0:     1;
		// Descriptor Privilege Level
		_64bit dpl:             2;
		// present
		_64bit p:               1;
		_64bit segment_16_19:	4;
		// available for use by system software
		_64bit available:		1;
		_64bit __zero1:			2;
		// granularity
		_64bit g:				1;
		_64bit offset_24_63:    40;
		_64bit __zero2: 	    32;
		/*
			reserved3: 8;
			__zero0:  5;
			reserved4: 19;
		*/
}  __attribute__((__packed__)) tss_descriptor;

typedef struct _64bit_tss {
	_32bit __reserved0;
	tss_descriptor rsp0;
	tss_descriptor rsp1;
	tss_descriptor rsp2;
	_64bit __reserved1;
	tss_descriptor ist1;
	tss_descriptor ist2;
	tss_descriptor ist3;
	tss_descriptor ist4;
	tss_descriptor ist5;
	tss_descriptor ist6;
	tss_descriptor ist7;
	_64bit __reserved2;
	_16bit __reserved3;
	_16bit __iomap_base_address;
} _64bit_tss;

typedef struct _64bit_registers {
	_64bit _rax;
	_64bit _rbx;
	_64bit _rcx;
	_64bit _rdx;

	_64bit _rsi;
	_64bit _rdi;

	_64bit _rbp;
	_64bit _rsp;
	
	_64bit _r8;
	_64bit _r9;
	_64bit _r10;
	_64bit _r11;
	_64bit _r12;
	_64bit _r13;
	_64bit _r14;
	_64bit _r15;

	_64bit _rip;
} __attribute__((__packed__)) _64bit_registers;

typedef void (*_process_function)();


typedef struct process {
	_64bit* rsp;
	_64bit* stack;
	
	struct process* next;
	struct process* prev;

	
	_64bit status;
	char* name;
	_64bit pid;
	_64bit start_time;


	/** useless crap */
	_64bit_registers registers;
	_process_function _init;
	_64bit_tss tss;
	
} process;



void
process_initialise();

void
process_idle();

void
pm_schedule();


_64bit kernel_stack[8192];
#define PROCESS_READY		0
#define PROCESS_SLEEP		1
#define PROCESS_STANDBY		2


#endif