#include <types.h>
#ifndef INTERRUPTS_H
#define INTERRUPTS_H

#define CLOCK_IRQ 	 	0x0
#define KEYBOARD_IRQ	0x1

#define IST_1 			1


#define DPL_KERNEL		0
#define DPL_DRIVERS		1
#define DPL_SERVERS		2
#define DPL_USER		3

/** Intel Manual Vol.3 3-19  */
#define _64BIT_LDT                  2
#define _64BIT_TSS_AVAILABLE        9
#define _64BIT_TSS_BUSY             11
#define _64BIT_CALL_GATE            12
#define _64BIT_INTERRUPT_GATE       14
#define _64BIT_TRAP_GATE            15






void
interrupt_hardware_setup();


void
interrupt_hardware_setup_handler(_8bit number,_64bit handler, _8bit ist, _8bit idl_type);

void
interrupt_initialise();

void
interrupt_register(_8bit number,void (*handler) (void));

void
interrupt_unregister(_8bit number);

void
interrupt_handler(_8bit number);

//void interrupt_handler_register(_32bit number,_64bit function_address,_8bit ist, _8bit dpl_type);



void interrupt_hardware_load_idt();
void interrupt_hardware_disable();
void interrupt_hardware_enable();

void interrupt_enable(_8bit irq_number);
void interrupt_disable(_8bit number);

void _hw_int_0(void);
void _hw_int_1(void);
void _hw_int_2(void);
void _hw_int_3(void);
void _hw_int_4(void);
void _hw_int_5(void);
void _hw_int_6(void);
void _hw_int_7(void);
void _hw_int_8(void);
void _hw_int_9(void);
void _hw_int_10(void);
void _hw_int_11(void);
void _hw_int_12(void);
void _hw_int_13(void);
void _hw_int_14(void);
void _hw_int_15(void);



#endif
