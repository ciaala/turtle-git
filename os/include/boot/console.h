

void print_char(char value);
void print_string(char* string);
void println_string(char* string);

#define print_hex(number) \
	print_char('0'); print_char('x'); _print_number(number,16)

#define print_int(number) \
	_print_number(number,10)

