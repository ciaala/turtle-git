#define ROWS 512

#define CCAT(high,middle,low) high ## middle ## low ## llu

/* 36 bit mask to get the 4kb page aligned addre */
#define PAGE_TABLE_ADDRESS_MASK	0xFFFFFF000llu
#define _4KB_PAGE_ADDRESS_MASK  0xFFFFFF000llu
#define _2MB_PAGE_ADDRESS_MASK  0xFFFE00000llu
#define _1GB_PAGE_ADDRESS_MASK  0xFC0000000llu



#define PAGE_flags_EXECUTABLE 0x80000000llu
#define PAGE_rw_READABLE 0x1llu
#define PAGE_rw_WRITABLE 0x3llu
#define PAGE_us_USER 0x0llu
#define PAGE_us_SUPERVISOR 0x4llu
#define PAGE_flags_WRITE_TROUGH 0x8llu
#define PAGE_flags_CACHABLE 0x10llu
#define PAGE_flags_EMPTY_FLAG 0x0llu


typedef unsigned long int _32bit;



typedef union _64bit {
  unsigned long long field;
  struct {
	_32bit low;
  	_32bit high;
  };
} __attribute__ ((__packed__)) _64bit ;


typedef _64bit mm_page_table_row;

typedef struct mm_page_table{
	mm_page_table_row rows[ROWS];
} mm_page_table;

// extern mm_page_table*
// mm_internal_setup_identity_map(_32bit physical, _32bit* low_index, _32bit* high_index, _32bit pages, mm_page_table* free_tables);

extern void
mm_internal_prepare_table(mm_page_table* main_table, mm_page_table** table_depth, _32bit* index, _32bit physical, int pages);

extern void
mm_setup_identity_map();
