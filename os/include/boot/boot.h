#define MULTIBOOT_HEADER_MAGIC 0x1BADB002
//#define MULTIBOOT_HEADER_MAGIC 0xe85250d6
#define MULTIBOOT_HEADER_FLAGS 0x00000003
#define MULTIBOOT_HEADER_CHECKSUM -(MULTIBOOT_HEADER_FLAGS + MULTIBOOT_HEADER_MAGIC)
// Virtual Address of the Kernel Base
#define kernel_VMA_BASE (0xffffffffc0000000)
// Linear Address of the Kernel Base
#define kernel_LMA_BASE (0x100000)


#define CR4_PAE 	0x0020
#define CR4_PSE 	0x0010
#define EFER_LME 	0x0100
#define EFER_SCE	0x1
#define EFER_NXE	0x800
#define MSR_EFER	0xc0000080
// paging
#define CR0_PG		0x80000000
// protected environment
#define CR0_PE		0x00000001






#define s_print(X) \
	movl X , %eax; \
	movl %eax, (%esp); \
	call print_string;

#define s_println(X) \
	movl X , %eax; \
	movl %eax, (%esp); \
	call println_string; 

#define _call_1(method, X) \
	movl X, %eax; \
	movl %eax, (%esp); \
	call method;

#define stack64bit(X,delta)\
	movl X, %ebx; \
	movl 4(%ebx), %eax; \
	movl (%ebx), %edx; \
	movl %eax, delta(%esp); \
	movl %eax, 4+delta(%esp);
