#include <types.h>
#ifndef MEMORY_LIST_H
#define MEMORY_LIST_H



typedef struct memory_list_element{
	struct memory_list_element* next;
	struct memory_list_element* prev;
	_64bit low_address;
	_64bit high_address;
	_64bit size;
} memory_list_element;



typedef struct memory_list_head{
	_32bit length;
	memory_list_element* first;
} memory_list_head;



memory_list_head*
memory_list_head_new();

void
memory_list_add(memory_list_head* head, _64bit low_address, _64bit high_address );


#endif