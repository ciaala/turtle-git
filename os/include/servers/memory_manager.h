#include <boot/multiboot.h>
#include <servers/memory_list.h>

#ifndef MEMORY_MANAGER_H
#define MEMORY_MANAGER_H



#define PAGE_SIZE			0x200000ull
#define INITIAL_STACK_SIZE	0x200000ull




typedef struct memory_manager{
	memory_list_head freelist;
	memory_list_head usedlist;
} memory_manager;

typedef struct mm_page_table{
	_64bit rows[512];	
} mm_page_table;

extern mm_page_table mm_main_paging_table;



memory_manager*
memory_manager_initialise(struct grub_multiboot_info* mb_info);

void*
kalloc(_32bit length);

void
kfree(void* address);

void*
kalloc_page();

void*
kalloc_stack();




void
kmemcpy8( _64bit* source, _64bit* target, _64bit size);

void
kmemset8( _64bit* address, _64bit size, _64bit value);


void
kmemcpy( char* source, char* target, _64bit size);

void
kmemset( char* source, _64bit size, _8bit value);



void
kmm_map_page(_64bit address);


#endif
