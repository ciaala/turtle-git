#ifndef TYPES_H
#define TYPES_H
typedef unsigned long long int	_64bit;
typedef unsigned long int		_32bit;
typedef unsigned short int		_16bit;
typedef unsigned char			_8bit;

#define NULL 0

#endif