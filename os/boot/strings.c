const char* s_OS_VERSION = "Turtle OS version 0 ";
const char* s_EFER_LME = "[efer.lme]";
const char* s_PAE = "[pae]";
const char* s_PT4 = "[pt4]";
const char* s_PAGING= "[paging]";
const char* s_GDT = "[gdt]";
const char* s_IDT = "[idt]";
const char* s_IDENTITYMAP = "[identity map]";
const char* s_64BIT = "[64BIT]";
const char* s_NEWLINE = "\n";

#define STRINGIFY_(x) #x
#define STRINGIFY(x) STRINGIFY_(x)
const char* s_COMPILE_TIME = STRINGIFY(__COMPILETIME__);