// #define console_base 0xb8000
// #define col 0x50
// #define row 0x32

int row=0;
int column=0;
int max_row=25;
int max_column=80;
char* base;
char* p;
char* console_wellcome="Early Console v.0";

void print_char(char c);
void scroll_line(int rows);
void increment_line();

void print_string(char* string){
	while (*string){
		print_char(*string);
		string++;
	}
}
void println_string(char* string){
	while(*string){
		print_char(*string);
		string++;
	}
	print_char('\n');
}


void print_char(char c){
	if (c == '\n'){
	  	increment_line();
	}
	else {
		if (column >= max_column) {
		  increment_line();
		}	
		*p = c; 
		p += 2;
		column++;
	}
}

void increment_line(){
	column = 0;
	if ( row > max_row){
		scroll_line( row - max_row + 1 );
		row = max_row--;
	}
	row ++;
	p = base + (row * max_column *2);
}

void scroll_line(int rows){
	int i;
	char* source = base + 2*rows*max_column;
	char* target = base;
	for (i = (max_row - rows)*max_column; i >0; i-- ){
		*target = *source;
		target+=2;
		source+=2;
	}
}

void console_reset(){
  int i;
  char* source = base;
  for ( i=max_row*max_column ; i>0; i--){
	  *source = 0x20;
	  source += 2;
  }
  column = 0;
  row = 0;
  p = base;
}

void _print_number(unsigned int value, int base) {
	char ascii[24];
	char x2c[] = "0123456789ABCDEF";
	char *c = ascii + sizeof(ascii) - 1;
	*c = 0;
	do {
		c--;
		*c = x2c[(value % base)];
	} while ( (value/=base)>0 );
	print_string(c);
}
