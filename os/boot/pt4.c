#pragma src
#include <boot/pt4.h>
#include <boot/console.h>

extern mm_page_table mm_main_paging_table;
mm_page_table* free_tables;

_32bit
prepare_free_table() {
    mm_page_table* new_table = free_tables;
    // let's advance the pointer to the next free table
    free_tables ++;
	_32bit *elements = (_32bit*)new_table;
    int i;
    for ( i=0; i < 1024; i++) {
  		elements[i] = 0;
    }
    return (_32bit)new_table;
}

void
mm_setup_identity_map() {
    mm_page_table *main_table = 0, *low_table_depth[2], *high_table_depth[2];
    _32bit low_index[2], high_index[2];
    free_tables = &mm_main_paging_table;
    low_index[0] = 0;
    low_index[1] = 0;
//  low_index[2] = 0;
    high_index[0] = 511;
    high_index[1] = 511;
//	high_index[2] = 0;
    _32bit physical = 0x0;
//	identity mapped
	main_table = (mm_page_table *)prepare_free_table();
	println_string(" ");
	println_string("Setup Identity Map");
	mm_internal_prepare_table(main_table, low_table_depth, low_index, physical, 1);
	mm_internal_prepare_table(main_table, high_table_depth, high_index, physical, 1);
;
}

_64bit
mm_build_page_table_row( _32bit address, _32bit low_flag, _32bit high_flag) {
    _64bit result;
    _32bit low_address_mask = 0xFFFFF000;
    _32bit high_address_mask = 0xFF;
    result.low = (address & low_address_mask) | low_flag;
	//result.high= (address &high_address_mask) | high_flag;
	result.high = high_flag;
    return result;
}
_64bit
mm_build_1GB_page_row(_32bit address, _32bit low_flag, _32bit high_flag) {
	_64bit result;
	_32bit low_address_mask = 0xC0000000;
	_32bit high_address_mask = 0xFF;
	low_flag = low_flag | 0x80;
	result.low = (address & low_address_mask) | low_flag;
	//result.high= (address &high_address_mask) | high_flag;
	result.high = high_flag;
	return result;
}

_64bit
mm_build_2MB_page_row(_32bit address, _32bit low_flag, _32bit high_flag) {
    _64bit result;
    _32bit low_address_mask = 0xFFE00000;
    _32bit high_address_mask = 0xFF;
	low_flag = low_flag | 0x80;
    result.low = (address & low_address_mask) | low_flag;
    //result.high= (address &high_address_mask) | high_flag;
	result.high = high_flag;
    return result;
}

_32bit
mm_internal_setup_new_sub_directory(mm_page_table* directory, int row_index) {

	_32bit low_flag = PAGE_rw_WRITABLE | PAGE_us_SUPERVISOR;
	mm_page_table_row *row = &(directory->rows[row_index]);
	// offset
	mm_page_table *sub_directory = (mm_page_table*) prepare_free_table();
    row->field = mm_build_page_table_row( (_32bit)sub_directory, low_flag, 0).field;
    return (_32bit) sub_directory;
}

void
mm_internal_setup_page_frames(_32bit physical, mm_page_table* last_directory, int page_frames)
{
    mm_page_table_row* row;
	_32bit low_flag = PAGE_rw_WRITABLE | PAGE_us_SUPERVISOR;
//    _32bit ex = PAGE_flags_EXECUTABLE;
    int i;
    for (i=0;i < page_frames; i++ ) {
        row = &(last_directory->rows[i]);
        row->field = mm_build_2MB_page_row( physical, low_flag, 0).field;
		//row->field = mm_build_1GB_page_row( physical, low_flag, 0).field;
		physical += 0x200000;
		print_string("| -");
		print_int( i );
		print_string("- ");
		print_hex(physical);
		print_char('\n');
    }
}

void
mm_internal_prepare_table(mm_page_table* main_table, mm_page_table** table_depth, _32bit* index, _32bit physical, int pages) {
// 	int i;
// 	_32bit address;
	mm_page_table* table0 = table_depth[0];
	mm_page_table* table1 = table_depth[1];
	//mm_page_table* table2 = table_depth[2];
    table0 = (mm_page_table*) mm_internal_setup_new_sub_directory(main_table, index[0]);
    table1 = (mm_page_table*) mm_internal_setup_new_sub_directory(table0,index[1]);
	
    mm_internal_setup_page_frames(physical, table1, pages);
}
