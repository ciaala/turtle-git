#include "types.h"
#include "servers/memory_manager.h"
#include <string.h>

struct {
unsigned fg_color:
    4;
unsigned bg_color:
    4;
unsigned value:
    8;
} char_clean;

typedef struct early_console {
    char* cursor;
    char* base;

    _64bit column;
    _64bit row;

    _64bit clean_tile;

    
    _64bit max_column;
    _64bit max_row;
    _64bit size;
    _64bit row_size;
} early_console;

early_console ec;


/** PRIVATE */
void
_console_carriage_return() {
    // moving the cursor
    ec.cursor = ec.row*ec.row_size + ec.base;
    ec.column = 0;
    // blanking the line 
    kmemset8( (_64bit*)ec.cursor, ec.row_size, ec.clean_tile);

}
void
_console_increment_line(){
    ec.row ++;
    if (ec.row >= ec.max_row) {
        ec.row = ec.max_row-1;
        _64bit scrolled_size = ec.row_size * ec.row;
        _64bit* source = (_64bit*)(ec.base + ec.row_size);
        kmemcpy8( source,(_64bit*)ec.base, scrolled_size );
        source =(_64bit*)(ec.base + scrolled_size);
        kmemset8( source, ec.row_size, ec.clean_tile );
      
    }
    ec.column = 0;
    ec.cursor = ec.row * ec.row_size + ec.base;
}

void
console_print_char(char c){
    switch (c)
    {
       case '\n':
            _console_increment_line();
            break;
       case '\r':
            _console_carriage_return();
            break;
            
        default:{
            if ( ec.column >= 80 ) {
                _console_increment_line();
            }
            *(ec.cursor) = c;
            ec.cursor += 2;
            ec.column ++;
            
        }   
    }
}

console_print_string(char* string){
    while (string && *string) {
        console_print_char(*string);
        string++;
    }
}


console_println_string(char* string){
    while (string && *string) {
        console_print_char(*string);
        string++;
    }
    console_print_char('\n');
}
void
_console_print_number(_64bit value, _64bit base) {
    char ascii[24];
    char x2c[] = "0123456789ABCDEF";
    char *c = ascii + sizeof(ascii) - 1;
    *c = 0;
    do {
        c--;
        *c = x2c[(value % base)];
    } while ( (value/=base)>0 );
    console_print_string(c);
}

void
_64_print_char(char c) {
   console_print_char(c);
}

void
_64_println_string(char* string) {
    console_println_string(string);
}

void
_64_print_string(char* string) {
    console_print_string(string);
}



void
_console_reset() {
    kmemset8( (_64bit*)ec.base, ec.size, ec.clean_tile );
    ec.column = 0;
    ec.row = 0;
    ec.cursor = ec.base;
}



void
console_initialise(char* base_address){
    ec.base = base_address;
    ec.cursor = base_address;
    ec.max_column = 80;
    ec.max_row = 25;
    ec.row_size = 2*ec.max_column;
    ec.size = ec.row_size*ec.max_row;
    ec.clean_tile = 0x0720072007200720;
    _console_reset();
}

// void
// kmemcpy8( _64bit* source, _64bit* target, _64bit size){
//     memcpy(target,source, size);
// }
// 
// void
// kmemset8(_64bit* address, _64bit size, _64bit value){
//     memset(address,value,size);
// }
// void
// kmemcpy( char* source, char* target, _64bit size) {
//     while ( size > 0 ) {
//         size--;
//         *target = *source;
//         target++;
//         source++;
//     }
// }
// 
// void
// kmemcpy8( _64bit* source, _64bit* target, _64bit size){
//     while ( size > 0 ) {
//         size-=8;
//         *target = *source;
//         target++;
//         source++;
//     }
// }
// 
// void
// kmemset( char* address, _64bit size, _8bit value) {
//     while ( size > 0 ) {
//         size--;
//         *address = value;
//         address++;
//     }
// }
// 
// void
// kmemset8(_64bit* address, _64bit size, _64bit value){
//     while( size > 0 ){
//         *address = value;
//         address ++;
//         size -= 8;
//     }
// }
// kate: indent-mode cstyle; space-indent on; indent-width 0; 
